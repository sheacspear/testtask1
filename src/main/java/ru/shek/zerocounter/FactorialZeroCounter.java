package ru.shek.zerocounter;

public interface FactorialZeroCounter {

    int count(int n);

}
