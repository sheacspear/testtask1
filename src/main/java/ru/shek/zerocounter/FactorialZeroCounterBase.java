package ru.shek.zerocounter;

import com.google.common.base.Preconditions;

public class FactorialZeroCounterBase implements FactorialZeroCounter {

    @Override
    public int count(int n) {
        Preconditions.checkArgument(n >= 0);
        int cnt = 0;
        //add additional condition i > 0, because i * 5 may be > Integer.MAX_VALUE
        //negative start from 0x80000000 @see java.lang.Integer.MIN_VALUE
        for (int i = 5; i <= n && i > 0; i = i * 5) {
            cnt = cnt + (n / i);
        }
        return cnt;
    }
}
