package ru.shek.zerocounter.test;

import org.junit.Assert;
import org.junit.Test;
import ru.shek.zerocounter.FactorialZeroCounterBase;

import java.math.BigInteger;

public class SlowFactorialZeroCounterBaseTest {

    private static final FactorialZeroCounterBase factorialZeroCounterBase = new FactorialZeroCounterBase();

    private static final int MAX_FACTORIAL = 10000;

    @Test
    public void test() {
        int n = MAX_FACTORIAL;
        BigInteger factorial = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
            Assert.assertEquals("i = " + i + " factorial = " + factorial, zeroCount(factorial), factorialZeroCounterBase.count(i));
        }
    }

    //zero counting from tail
    private int zeroCount(BigInteger r) {
        char[] data = r.toString(10).toCharArray();
        int cnt = 0;
        for (int i = data.length - 1; i >= 0 && data[i] == '0'; i--) {
            cnt++;
        }
        return cnt;
    }
}
