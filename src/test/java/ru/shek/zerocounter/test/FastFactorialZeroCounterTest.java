package ru.shek.zerocounter.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ru.shek.zerocounter.FactorialZeroCounterBase;

@RunWith(value = Parameterized.class)
public class FastFactorialZeroCounterTest {


    private static final FactorialZeroCounterBase factorialZeroCounterBase = new FactorialZeroCounterBase();

    private final int n;

    private final int countZero;

    public FastFactorialZeroCounterTest(int n, int countZero) {
        this.n = n;
        this.countZero = countZero;
    }

    @Parameterized.Parameters(name = "{index}: n = {0}, countZero = {1}")
    public static Object[][] data() {
        return new Object[][]{
                {0, 0},
                {1, 0},
                {2, 0},
                {5, 1},
                {1000, 249}
        };
    }

    @Test
    public void test() {
        Assert.assertEquals(countZero, factorialZeroCounterBase.count(n));
    }
}
